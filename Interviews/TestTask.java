import java.util.*;
import java.util.stream.Collectors;

public class TestTask {

    public void printWords (List<String> str) {
        List<String> strTmp = new ArrayList<>();
        HashMap <String, Integer> countWords = new HashMap<>();
        for (String word : str) {
            if ( countWords.containsKey(word.toLowerCase())) {
                countWords.put(word.toLowerCase(), countWords.get(word.toLowerCase()) + 1);
            } else {
                countWords.put(word.toLowerCase(), 1);
            }
        }
        for (String word : countWords.keySet()) {
            String s = Character.toUpperCase(word.charAt(0)) + word.toLowerCase().substring(1);
            strTmp.add(s);
        }
        Collections.sort(strTmp);
        for (String word : strTmp){
            Integer value = countWords.get(word.toLowerCase());
            System.out.println(word + ":" + value);
        }
    }
}
