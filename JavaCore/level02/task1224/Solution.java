package level02.task1224;

public class Solution {
    public static void main(String[] args) {
        System.out.println(getObjectType(new Cat()));
        System.out.println(getObjectType(new Tiger()));
        System.out.println(getObjectType(new Lion()));
        System.out.println(getObjectType(new Bull()));
        System.out.println(getObjectType(new Pig()));
    }

    public static String getObjectType(Object o) {
        if (o instanceof Cat)//напишите тут ваш код
            return "Кот";
        if (o instanceof Tiger)//напишите тут ваш код
            return "Тигр";
        if (o instanceof Lion)//напишите тут ваш код
            return "Лев";
        if (o instanceof Bull)//напишите тут ваш код
            return "Бык";
        return "Животное";
    }

    public static class Cat {
    }

    public static class Tiger {
    }

    public static class Lion {
    }

    public static class Bull {
    }

    public static class Pig {
    }
}
