package level03.task1307;

public class Solution {
    public static void main(String[] args) throws Exception {
    }

    interface SimpleObject<T> {
        SimpleObject<T> getInstance();
    }

    class StringObject implements SimpleObject<String> {
        public SimpleObject<String> getInstance() {return new StringObject();}
    }
}
