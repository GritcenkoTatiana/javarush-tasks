# Решение задач с сайта JavaRush

## JavaSyntax
### 10 уровень
* [Двоичный конвертер](JavaSyntax/lecture10/task01)


## JavaCore
### 1 уровень
* [Минимакс](JavaCore/level01/task1123)


### 2 уровень
* [То ли птица, то ли лампа](JavaCore/level02/task1204)
* [Определимся с животным](JavaCore/level02/task1205)
* [Все мы немного кошки…](JavaCore/level02/task1221)
* [Больше не Пушистик](JavaCore/level02/task1222)
* [Неведома зверушка](JavaCore/level02/task1224)
* [Посетители](JavaCore/level02/task1225)
* [Попадание в десятку](JavaCore/level02/task1230)
* [Изоморфы наступают](JavaCore/level02/task1233)


### 3 уровень
* [Пиво](JavaCore/level03/task1301)
* [Четыре ошибки](JavaCore/level03/task1305)
* [Баг в initializeIdAndName](JavaCore/level03/task1306)
* [Параметризованый интерфейс](JavaCore/level03/task1307)
